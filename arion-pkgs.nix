# import <nixpkgs> { system = "x86_64-linux"; }

# FROM: https://github.com/hercules-ci/arion/blob/main/examples/flake/arion-pkgs.nix
let
  flake =
    if builtins ? getFlake
    then (builtins.getFlake (toString ./.)).pkgs-for-arion
    else (import flake-compat { src = ./.; }).defaultNix;
  # NB: this is lazy
  lock = builtins.fromJSON (builtins.readFile ./flake.lock);
  inherit (lock.nodes.flake-compat.locked) owner repo rev narHash;
  flake-compat = builtins.fetchTarball {
    url = "https://github.com/${owner}/${repo}/archive/${rev}.tar.gz";
    sha256 = narHash;
  };
in
flake.pkgs
