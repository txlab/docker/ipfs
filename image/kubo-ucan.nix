{ pkgs, inputs, inputs', ... } @ args:
with pkgs.lib;
with import ../lib/s6.nix args;
with import ../lib/utils.nix args;
let
  kuboService = import ../service/kubo.nix args;

  # Cloudron makes /var read-only, but s6-overlay requires /var/run to link to /run
  varRunLink = pkgs.runCommand "var-run-link" { } ''
    mkdir -p $out/var
    ln -s /run $out/var/run
  '';
in
rec {

  # ? kuboService.servicesConfig // 
  servicesConfig = {
    "ucan-proxy" = {
      type = "longrun";
      run = ''
        #!/command/execlineb -P
        pipeline -w { sed --unbuffered "s/^/[ucan-proxy] /" }
        fdmove -c 2 1 # redirect stderr into stdout
        exec ${getExe inputs'.ucan-store-proxy.packages.default}
      '';
      finish = finishWholeContainer;
    };
  };
  servicesDir = mkServiceDir servicesConfig;

  imageConfig = kuboService.imageConfig // {
    name = "ucan-host-ipfs";
    tag = "kubo-ucan"; # ? is this good practice?
    config = {
      # Entrypoint = [ "/init" ]; #TODO: entrypoint?
      Cmd = [ "/init" ];
      # Cmd = [ "sh" "-c" "id && ls -al /var && touch /var/run" ];

      Env = kuboService.imageConfig.config.Env ++ [
        "UCAN_KUBO_RPC=http://localhost:5001"
        "UCAN_TRUSTED_DIDS=todo"
        "UCAN_GATEWAY_PUBLIC_URL=http://localhost:8080"

        # S6 - https://github.com/just-containers/s6-overlay#customizing-s6-overlay-behaviour
        "S6_BEHAVIOUR_IF_STAGE2_FAILS=2" # stop container if one service fails
        "S6_KEEP_ENV=1" # otherwise all scripts & services would need with-contenv
        # "S6_READ_ONLY_ROOT=1" # HACK: cloudron /var/run...
      ];
      ExposedPorts = {
        "8000/tcp" = { }; # UCAN proxy
      };
      #TODO: healthcheck? or redundant with s6 healthcheck?
    };
    copyToRoot = with pkgs; kuboService.imageConfig.copyToRoot ++ [
      varRunLink
      # Environment helpers - https://nixos.org/manual/nixpkgs/stable/#ssec-pkgs-dockerTools-helpers
      dockerTools.fakeNss # basic users & /etc/hosts setup
      dockerTools.binSh # /bin/sh - for scripts
      # dockerTools.usrBinEnv # /usr/bin/env
      dockerTools.caCertificates

      s6-overlay
      servicesDir
      runConfigureScripts # This script is added to PATH so that it can be easily used when overriding init script
    ];

    layers = kuboService.imageConfig.layers;
    maxLayers = 15; # TODO optimize?

  };

  image = nix2container.buildImage imageConfig;
}
