{ pkgs, inputs, inputs', ... } @ args:
with pkgs.lib;
with import ../lib/s6.nix args;
with import ../lib/utils.nix args;

let
  kubo-ucan = import ./kubo-ucan.nix args;

  # This script is at a static location so that it can be overridden by mount
  # TODO: Caddy letsencrypt email
  caddyInit = writeShellScriptToDir "/container-init.d/caddy/default" ''
    export # for debugging
    
    # Create caddyfile from env vars (if none is mounted)
    if [ ! -e "/etc/caddy/Caddyfile" ]; then
        # Ensure the IPFS_PUBLIC_URL environment variable is set
        if [ -z "$IPFS_PUBLIC_URL" ]; then
            echo "Error: IPFS_PUBLIC_URL environment variable is not set." >&2
            exit 1
        fi
        # Ensure the UCAN_PUBLIC_URL environment variable is set
        if [ -z "$UCAN_PUBLIC_URL" ]; then
            echo "Error: UCAN_PUBLIC_URL environment variable is not set." >&2
            exit 1
        fi

        echo "Creating Caddyfile..."
        mkdir -p /etc/caddy
        cat <<EOF | tee /etc/caddy/Caddyfile
    ''${IPFS_PUBLIC_URL} {
        reverse_proxy http://localhost:8080
    }
    ''${UCAN_PUBLIC_URL} {
        reverse_proxy http://localhost:8000
    }
    EOF
    else
        echo "Caddyfile already exists. No action taken."
    fi

    # run other configure scripts (if they exist)
    #${runConfigureScripts.name} /container-init.d/caddy
  '';
in
rec {

  servicesConfig = {
    #kubo-ucan.servicesConfig // {
    "caddy" = {
      type = "longrun";
      "dependencies.d" = {
        "caddy-init" = ""; #TODO: improve syntax
      };
      run = ''
        #!/command/execlineb -P
        pipeline -w { sed --unbuffered "s/^/[caddy] /" }
        fdmove -c 2 1 # redirect stderr into stdout
        exec ${getExe pkgs.caddy} run --config /etc/caddy/Caddyfile
      '';
      finish = finishWholeContainer;
      # TODO:healthcheck
    };
    "caddy-init" = {
      type = "oneshot";
      up = "/container-init.d/caddy/default";
    };
  };
  servicesDir = mkServiceDir servicesConfig;

  imageConfig = kubo-ucan.imageConfig // {
    name = "ucan-host-ipfs";
    tag = "kubo-ucan-caddy"; # ? is this good practice?
    config = kubo-ucan.imageConfig.config // {
      # Entrypoint = [ "/init" ]; #TODO: entrypoint?
      Cmd = [ "/init" ];
      # Env = kubo-ucan.imageConfig.config.Env ++ [      ];
      ExposedPorts = kubo-ucan.imageConfig.config.ExposedPorts // {
        # Caddy
        "80/tcp" = { };
        "443/tcp" = { };
      };
      #TODO: healthcheck? or redundant with s6 healthchecks?
    };
    copyToRoot = kubo-ucan.imageConfig.copyToRoot ++ [
      servicesDir
      caddyInit
    ];
  };

  image = nix2container.buildImage imageConfig;
}

