# Self-hosted kubo & more

### **This is a WIP [based on this post](https://discuss.ipfs.tech/t/all-in-one-docker-image-with-ipfs-node-best-practices/17408)**
Planning to provide a simple way to self-host an IPFS node with:
- Kubo node (for retrieving & providing content)
- (optional) HTTP Gateway
- (optional) UCAN-authenticated upload API ([ucan-store-proxy](https://gitlab.com/wovin/deploy/ucan-store-proxy/))

# Overview
- [Quick start](#quick-start) - setting up your IPFS node
- [Kubo](#kubo) - profile, config, usage

## Quick start
1. [Choose kubo profile](#choose-profile)
2. Start docker containers:

### Start
```bash
docker-compose up  # add `-d` if you want to run in background
```

## Kubo
#### Choose profile
Before first start, you *can* set a profile in `docker-compose.yml`: `services > kubo > variables`. Some examples:
- **no profile (default)** — if you want LAN discovery and automatically provide content in your store if you are reachable from the internet. (detected automatically)
- **`server`** — if you host in a datacenter with a public IP, and don't want LAN discovery
- **`lowpower`** — lower resource usage - if you don't want to provide (host) content, and performance is not so important

[Read more in the official docs](https://github.com/ipfs/kubo/blob/master/docs/config.md#profiles).

### Config
Add custom config to `config/kubo-config.sh`.  
[More documentation can be found in the official docs](https://docs.ipfs.tech/install/run-ipfs-inside-docker/)

#### CLI
```bash
# Run IPFS CLI inside docker container:
./kubo-cli.sh stats repo
# Connect your local CLI with the running node (replace with the IP of your server):
export IPFS_API=/ip4/127.0.0.1/tcp/5001
ipfs stats repo
```

