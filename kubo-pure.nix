{ pkgs, nix2container, ... }:
let
  kuboFS = pkgs.runCommand "mkdir-kubo" { } ''
    mkdir -p $out/app/data
  '';
in
nix2container.buildImage {
  name = "ucan-host-ipfs";
  copyToRoot = with pkgs; [
    # Environment helpers - https://nixos.org/manual/nixpkgs/stable/#ssec-pkgs-dockerTools-helpers
    dockerTools.fakeNss # basic users & /etc/hosts setup
    dockerTools.binSh # interactive sh (not bash)
    dockerTools.usrBinEnv # /usr/bin/env
    bashInteractive
    kuboFS
  ];
  config = {
    Env = [
      "IPFS_PATH=/app/data"
    ];
    Cmd = [
      "${pkgs.lib.getExe pkgs.kubo}"
      "daemon"
      "--init"
      "--migrate"
      # "--agent-version-suffix=docker"
    ];

    ExposedPorts = {
      "4001/tcp" = { };
      "4001/udp" = { };
      "5001/tcp" = { };
      "8080/tcp" = { };
    };


  };
  maxLayers = 15;
}
# dockerTools.buildImage {
#   name = "ucan-host-ipfs";
#   contents = [ ];
#   copyToRoot = pkgs.buildEnv {
#     paths = with pkgs; [
#       bash
#     ];
#     name = "foo-root";
#     pathsToLink = [ "/bin" ];
#   };
# }

