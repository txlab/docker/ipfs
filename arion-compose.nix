{ pkgs, lib, ... }:
{
  project.name = "ucan-host-ipfs";
  services = {

    kubo = {
      # service.image = "ipfs/kubo";
      # image.enableRecommendedContents = true;
      image.contents = [ pkgs.kubo ];
      service.command = [ "${lib.getExe pkgs.kubo}" "daemon" "--migrate=true" "--agent-version-suffix=docker" ];

      service.environment = {
        IPFS_PATH = "/data/ipfs";
        IPFS_PROFILE = "/data/ipfs"; # https://github.com/ipfs/kubo/blob/master/docs/config.md#profiles
      };
      service.ports = [
        # 'Swarm' ports for connecting to the IPFS network - listens on all interfaces so it's remotely reachable (if you have a public IP).
        "4001:4001/tcp"
        "4001:4001/udp"
        # The following ports only listen on the loopback interface, so are not remotely reachable by default.
        # If you want to override these or add more ports, see https://docs.docker.com/compose/extends/
        #
        # API port, which includes admin operations, so don't make this publically accessible.
        "127.0.0.1:5001:5001"
        # HTTP Gateway
        "127.0.0.1:8080:8080"
      ];
      service.volumes = [
        "./config/kubo-config.sh:/container-init.d/kubo-config.sh:ro"
        "./ipfs-data:/data/ipfs"

        # if you want fuse mounts: (https://github.com/ipfs/kubo/blob/master/docs/fuse.md)
        #"ipfs_fuse:/ipfs"
        #"ipns_fuse:/ipns"
      ];
      #service.useHostStore = true; - might be faster, but will not build independent docker images
      service.stop_signal = "SIGINT";
    };
  };
}
