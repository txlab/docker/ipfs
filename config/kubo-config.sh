#!/bin/sh
set -euxo pipefail # https://vaneyckt.io/posts/safer_bash_scripts_with_set_euxo_pipefail/

echo Applying custom config

ipfs config --json API.HTTPHeaders.Access-Control-Allow-Origin '["http://ipfs.localhost:5001", "http://localhost:5001", "http://127.0.0.1:5001", "http://localhost:3000", "https://webui.ipfs.io"]'

