{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-23.11";
    nixpkgs-tennox.url = "github:tennox/nixpkgs";
    nixpkgs-unstable.url = "github:NixOS/nixpkgs/nixos-unstable";
    systems.url = "github:nix-systems/default"; # (i) allows overriding systems easily, see https://github.com/nix-systems/nix-systems#consumer-usage
    devenv.url = "github:cachix/devenv";
    nix2container.url = "github:tennox/nix2container";
    ucan-store-proxy = {
      type = "gitlab";
      owner = "wovin";
      repo = "deploy%2Fucan-store-proxy";
    };
    nix-container-images = {
      url = "github:cloudwatt/nix-container-images";
      flake = false;
    };
    kuboRepo = {
      url = "github:ipfs/kubo";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, devenv, systems, flake-parts, nix-container-images, ... } @ inputs: (
    flake-parts.lib.mkFlake { inherit inputs; } {
      systems = (import systems);
      imports = [
        inputs.devenv.flakeModule
      ];
      perSystem = { config, self', inputs', pkgs, system, lib, ... }: # perSystem docs: https://flake.parts/module-arguments.html#persystem-module-parameters
        let
          # pkgs = nixpkgs.legacyPackages.${system};
          latest = inputs'.nixpkgs-unstable.legacyPackages;
          # nix2container = inputs'.nix2container.packages.nix2container;
        in
        {
          _module.args.pkgs = import inputs.nixpkgs {
            inherit system;
            overlays = [
              (final: prev: {
                inherit (inputs'.nixpkgs-tennox.legacyPackages) s6-overlay;
              })
            ];
          };

          packages = {
            dockerKuboUcan = (import ./image/kubo-ucan.nix { inherit pkgs inputs inputs'; }).image;
            dockerKuboUcanCaddy = (import ./image/kubo-ucan-caddy.nix { inherit pkgs inputs inputs'; }).image;
            # dockerKuboOnly = (import ./image/all-pure.nix { inherit pkgs inputs inputs'; }).image;
            # dockerUcanProxy = (import ./image/all-pure.nix { inherit pkgs inputs inputs'; }).image;

            # svcDir = (import ./image/all-pure.nix { inherit pkgs inputs inputs'; }).serviceDir;
            arion-compose-file = pkgs.arion.build { modules = [ ./arion-compose.nix ]; pkgs = import ./arion-pkgs.nix; };
          };
          devenv.shells.default = (import ./devenv.nix { inherit pkgs latest inputs; });
        };
      flake = {
        pkgs-for-arion = nixpkgs.legacyPackages.x86_64-linux;
      };
    }
  );

  nixConfig = {
    extra-substituters = [ "https://devenv.cachix.org" ];
    extra-trusted-public-keys = [ "devenv.cachix.org-1:w1cLUi8dv3hnoSPGAuibQv+f9TZLr6cv/Hm9XgU50cw=" ];
  };
}
