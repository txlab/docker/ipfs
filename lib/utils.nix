{ pkgs, inputs, ... }:
{
  inherit (inputs.nix2container.packages.${pkgs.system}) nix2container;

  # Mix between:
  # - writeTextDir      https://github.com/NixOS/nixpkgs/blob/master/pkgs/build-support/trivial-builders/default.nix#L189
  # - writeShellScript  https://github.com/NixOS/nixpkgs/blob/master/pkgs/build-support/trivial-builders/default.nix#L209
  writeShellScriptToDir = path: text:
    pkgs.writeTextFile {
      name = builtins.baseNameOf path;
      executable = true;
      destination = path;
      text = ''
        #!${pkgs.runtimeShell}
        set -euxo pipefail

        ${text}
      '';
      checkPhase = ''
        ${pkgs.stdenv.shellDryRun} "$target"
      '';
    };


  # Runs all scripts in directory (common for docker customization)
  runConfigureScripts = pkgs.writeShellApplication {
    name = "run-configure-dir-scripts";
    text = ''
      echo "Checking for scripts in $1"
      if [ -d "$1" ]; then
        # from: https://github.com/ipfs/kubo/blob/master/bin/container_daemon#L53C1-L53C113
        find "$1" -maxdepth 1 -type f -iname '*.sh' -print0 | sort -z | xargs -n 1 -0 -r ${inputs.kuboRepo}/bin/container_init_run
      fi    
    '';
  };
}
