{ pkgs, ... }:
with pkgs.lib;
let
  mkServiceDir = services:
    pkgs.symlinkJoin {
      name = "services";
      paths = mapAttrsRecursive [ ] services;
      postBuild = ''
        mkdir -p $out/etc/s6-overlay/s6-rc.d/user/contents.d
        for service in ${concatStringsSep " " (attrNames services)}; do
          touch $out/etc/s6-overlay/s6-rc.d/user/contents.d/''$service # HACK
        done
        ${getExe pkgs.tree} $out
      '';
    };

  mapAttrsRecursive = path: attrSet: with pkgs.lib;
    concatLists (mapAttrsToList
      (name: value:
        if isAttrs value then
          mapAttrsRecursive (path ++ [ name ]) value
        else
          [
            (pkgs.runCommand (concatStringsSep "-" (path ++ [ name ])) { } ''
              mkdir -p $out/etc/s6-overlay/s6-rc.d/${concatStringsSep "/" path}
              echo ${escapeShellArg value} > $out/etc/s6-overlay/s6-rc.d/${concatStringsSep "/" (path ++ [name])}
              chmod +x $out/etc/s6-overlay/s6-rc.d/${concatStringsSep "/" (path ++ [name])}
            '')
          ]
      )
      attrSet);
in
{
  inherit mkServiceDir;

  # https://github.com/just-containers/s6-overlay#writing-an-optional-finish-script
  finishWholeContainer = ''
    #!/command/execlineb -S0
    foreground { redirfd -w 1 /run/s6-linux-init-container-results/exitcode echo 0 }
    /run/s6/basedir/bin/halt
  '';
}
