{ pkgs, inputs, ... } @ args:
with pkgs.lib;
with import ../lib/s6.nix args;
with import ../lib/utils.nix args;
rec {

  # This script is at a static location so that it can be overridden by mount
  kuboInit = writeShellScriptToDir "/container-init.d/kubo/default" ''
    export # for debugging
    mkdir -p $IPFS_PATH
    
    if [ ! -e "$IPFS_PATH/config" ]; then
      #TODO: set default profile?
      ipfs init ''${IPFS_PROFILE:+"--profile=$IPFS_PROFILE"}
      # bind to all IPs:
      ipfs config Addresses.API /ip4/0.0.0.0/tcp/5001
      ipfs config Addresses.Gateway /ip4/0.0.0.0/tcp/8080
    fi
    if [ ! -z "$IPFS_ADDR" ]; then
      # ipfs config --json Addresses.AppendAnnounce "[\"$IPFS_ADDR\"]"
      ipfs config --json Addresses.Announce "[\"$IPFS_ADDR\"]"
      ipfs config Routing.Type dhtserver
      ipfs config Swarm.RelayClient.Enabled false
    fi

    ${runConfigureScripts.name} /container-init.d/kubo
    # also add /app/data/container-init.d for cloudron configurability
    ${runConfigureScripts.name} /app/data/container-init.d/kubo

    #TODO: Handle SWARM_{TCP,UDP}:
    # - set announce
    # - disable swarm if Cloudron && (SWARM_{TCP,UDP} is not present)
  '';

  servicesConfig = {
    "kubo" = {
      type = "longrun";
      "dependencies.d" = {
        "kubo-init" = ""; #TODO: improve syntax
      };
      run = ''
        #!/command/execlineb -P
        pipeline -w { sed --unbuffered "s/^/[kubo] /" }
        fdmove -c 2 1 # redirect stderr into stdout
        exec ipfs daemon

        # TODO ? use https://github.com/ipfs/kubo/blob/master/bin/container_daemon
        # exec ''${inputs.kuboRepo}/bin/container_daemon
      '';
      finish = finishWholeContainer;
      # TODO:healthcheck - https://github.com/ipfs/kubo/blob/master/Dockerfile#L105
    };
    "kubo-init" = {
      type = "oneshot";
      up = "/container-init.d/kubo/default";
    };
  };
  servicesDir = mkServiceDir servicesConfig;

  imageConfig = {
    config = {
      Env = [
        "IPFS_PATH=/app/data"
      ];
      ExposedPorts = {
        "4001/tcp" = { }; # swarm
        "4001/udp" = { };
        "5001/tcp" = { }; # RPC
      };
      #TODO: healthcheck? or redundant with s6 healthcheck?
    };
    copyToRoot = [
      servicesDir
      kuboInit
    ];

    layers = [
      (inputs.nix2container.packages.${pkgs.system}.nix2container.buildLayer {
        # FROM: https://github.com/nlewo/nix2container/blob/6aa8491e73843ac8bf714a3904a45900f356ea44/examples/bash.nix#L12
        # When we want tools in /, we need to symlink them in order to
        # still have libraries in /nix/store. This behavior differs from
        # dockerTools.buildImage but this allows to avoid having files
        # in both / and /nix/store.
        copyToRoot =
          (pkgs.buildEnv {
            name = "root";
            paths = with pkgs; [
              # for use in scripts (init/config):
              coreutils
              findutils
              # busybox # sed
              gnused
              ipfs
              runConfigureScripts
            ];
            pathsToLink = [ "/bin" ];
          })
        ;
      })
    ];
    maxLayers = 15; # TODO optimize?

  };

  image = nix2container.buildImage imageConfig;
}
